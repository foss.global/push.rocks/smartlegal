import { tap, expect } from '@pushrocks/tapbundle';
import * as legal from '../ts/index';

tap.test('should create instance of licenseChecker', async () => {
  const licenseChecker = await legal.createLicenseChecker();
  const plainResultArray = await licenseChecker.createPlainResultArray(process.cwd());
  expect(plainResultArray).to.be.instanceof(Array);
  expect(plainResultArray[0]).to.have.property('license');
});

tap.test('should exclude certain licenses', async () => {
  const licenseChecker = await legal.createLicenseChecker();
  const checkResult = await licenseChecker.excludeLicenseWithinPath(process.cwd(), ['MIT']);
  expect(checkResult.failingModules.length).to.be.greaterThan(10);
});

tap.test('should include certain licenses', async () => {
  const licenseChecker = await legal.createLicenseChecker();
  const checkResult = await licenseChecker.includeLicencesWithinPath(process.cwd(), ['MIT']);
  expect(checkResult.failingModules.length).to.be.greaterThan(10);
});

tap.start();
