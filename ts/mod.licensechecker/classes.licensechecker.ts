import * as plugins from '../legal.plugins';
import * as licenseChecker from 'license-checker';
import * as interfaces from './interfaces';
import { CheckResult } from './classes.checkresult';

export class LicenseChecker {
  async excludeLicenseWithinPath(pathArg: string, licenseArrayArg: string[]) {
    const checkResult = new CheckResult();
    const plainResultArray = await this.createPlainResultArray(pathArg);
    plainResultArray.forEach((licenseResult) => {
      if (licenseArrayArg.indexOf(licenseResult.license) === -1) {
        checkResult.addPassing(licenseResult);
      } else {
        checkResult.addFailing(licenseResult);
      }
    });
    return checkResult;
  }

  async includeLicencesWithinPath(pathArg: string, licenseArrayArg: string[]) {
    const checkResult = new CheckResult();
    const plainResultArray = await this.createPlainResultArray(pathArg);
    plainResultArray.forEach((licenseResult) => {
      if (licenseArrayArg.indexOf(licenseResult.license) !== -1) {
        checkResult.addPassing(licenseResult);
      } else {
        checkResult.addFailing(licenseResult);
      }
    });
    return checkResult;
  }

  async createPlainResultArray(pathArg: string) {
    const licenseJson = await this.getJsonForPath(pathArg);
    const resultArray: interfaces.IModuleLicenseResult[] = [];
    for (const moduleKey of Object.keys(licenseJson)) {
      const refObject = licenseJson[moduleKey];
      resultArray.push({
        moduleName: moduleKey,
        email: refObject.email,
        licenseFile: refObject.licenseFile,
        license: refObject.licenses,
        path: refObject.path,
        publisher: refObject.publisher,
        repository: refObject.repository,
      });
    }
    return resultArray;
  }

  private async getJsonForPath(checkPathArg) {
    const done = plugins.smartpromise.defer<any>();
    licenseChecker.init(
      {
        start: checkPathArg,
      },
      (err, licenseJson) => {
        if (err) {
          done.reject(err);
        } else {
          done.resolve(licenseJson);
        }
      }
    );
    return done.promise;
  }
}
