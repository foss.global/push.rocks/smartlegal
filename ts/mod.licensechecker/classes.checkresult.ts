import { IModuleLicenseResult } from './interfaces';

export class CheckResult {
  passingModules: IModuleLicenseResult[] = [];
  failingModules: IModuleLicenseResult[] = [];

  public addPassing(moduleResultArg: IModuleLicenseResult) {
    this.passingModules.push(moduleResultArg);
  }

  public addFailing(moduleResultArg: IModuleLicenseResult) {
    this.failingModules.push(moduleResultArg);
  }
}
