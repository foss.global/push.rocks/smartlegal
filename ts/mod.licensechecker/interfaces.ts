export interface IModuleLicenseResult {
  moduleName: string;
  license: string;
  repository: string;
  publisher: string;
  email: string;
  path: string;
  licenseFile: string;
}
