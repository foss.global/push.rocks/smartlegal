export * from './mod.licensechecker/interfaces';
export const createLicenseChecker = async () => {
  const licenseCheckerMod = await import('./mod.licensechecker/classes.licensechecker');
  return new licenseCheckerMod.LicenseChecker();
};
